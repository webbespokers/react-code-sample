import { IconName } from 'components/Icon'
import InputWithIcon from 'components/InputWithIcon'
import OnboardingForm, { style as formsStyle } from 'components/OnboardingForm'
import React from 'react'
import { getPasswordValidator }  from 'utils/common'
import { useDictionary }  from 'utils/hooks'

interface PasswordFormProps {
  title?: string
  subTitle?: string
  autoFocus?: boolean
  submitButtonLabel: string
  submitButtonIconName?: IconName
  isFormDisabled?: boolean
  isCurrentPasswordVisible?: boolean
  error?: Error | void
  onSubmit: (password: string, currentPassword: string) => Promise<void> | void
}

const passwordValidator = getPasswordValidator()

/**
 * Common component used to set or reset user's password.
 */
const PasswordForm = (props: PasswordFormProps) => {
  const {
    title,
    subTitle,
    error,
    submitButtonLabel,
    submitButtonIconName,
    autoFocus,
    isFormDisabled,
    isCurrentPasswordVisible,
    onSubmit,
  } = props
  const translate = useDictionary()
  const [currentPassword, setCurrentPassword] = React.useState('')
  const [password, setPassword] = React.useState('')
  const [repeatPassword, setRepeatPassword] = React.useState('')
  const [internalError, setInternalError] = React.useState<Error>()
  const isSubmitButtonDisabled = isFormDisabled || !password || !repeatPassword

  const internalSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    const isValidPassword = passwordValidator.validate(password)
    const arePasswordsEqual = password === repeatPassword

    if (!isValidPassword) {
      setInternalError(new Error(translate('password.form.error.invalid')))
    } else if (!arePasswordsEqual) {
      setInternalError(new Error(translate('password.form.error.notMatch')))
    } else {
      setInternalError(undefined)
    }

    if (isValidPassword && arePasswordsEqual) {
      onSubmit(password, currentPassword)
    }
  }

  return (
    <OnboardingForm
      label={title}
      submitButtonLabel={submitButtonLabel}
      submitButtonIconName={submitButtonIconName}
      isSubmitButtonDisabled={isSubmitButtonDisabled}
      onSubmit={internalSubmit}
      error={error || internalError}
    >
      {subTitle ? <div>{subTitle}</div> : null}
      {isCurrentPasswordVisible ? (
        <div className={formsStyle.inputRow}>
          <InputWithIcon
            required
            autoFocus={autoFocus}
            autoComplete="password"
            iconName="keys"
            placeholder={translate('password.form.currentPassword')}
            isDisabled={isFormDisabled}
            type="password"
            name="current-password"
            value={currentPassword}
            onChange={setCurrentPassword}
          />
        </div>
      ) : null}
      <div className={formsStyle.inputRow}>
        <InputWithIcon
          required
          autoFocus={autoFocus && !isCurrentPasswordVisible}
          autoComplete="new-password"
          iconName="keys"
          placeholder={translate('password.form.password')}
          isDisabled={isFormDisabled}
          type="password"
          name="password"
          value={password}
          onChange={setPassword}
        />
      </div>
      <div className={formsStyle.inputRow}>
        <InputWithIcon
          required
          autoComplete="new-password"
          iconName="keys"
          placeholder={translate('password.form.resetPassword')}
          isDisabled={isFormDisabled}
          type="password"
          value={repeatPassword}
          onChange={setRepeatPassword}
        />
      </div>
    </OnboardingForm>
  )
}

export default PasswordForm
