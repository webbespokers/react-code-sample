import PasswordForm from 'components/PasswordForm'
import SuccessMessage from 'components/SuccessMessage'
import React from 'react'
import { Link } from 'react-router-dom'
import { getUrlParams } from 'utils/common'
import { useApiAction, useDictionary }  from 'utils/hooks'

/**
 * Component displayed on the page opened from external link sent to user's email, to reset password.
 */
const ResetPasswordForm = () => {
  const translate = useDictionary()
  const [token = ''] = getUrlParams(['token'])
  const [password, setPassword] = React.useState('')
  const [
    apiAction,
    isApiActionInProgress,
    apiError,
    hasApiActionSucceeded,
  ] = useApiAction((api, signal) => api.resetPassword({ password, token }, { signal }))

  const callApiAction = async () => {
    await apiAction()
    setPassword('')
  }

  React.useEffect(() => {
    if (password) {
      callApiAction()
    }
  }, [password])

  return hasApiActionSucceeded ? (
    <SuccessMessage>
      {translate('password.form.resetSuccess.prefix')}
      <Link to="login">{translate('password.form.resetSuccess.login')}</Link>
      {translate('password.form.resetSuccess.suffix')}
    </SuccessMessage>
  ) : (
    <PasswordForm
      error={apiError}
      title={translate('password.form.resetPassword')}
      submitButtonLabel={translate('password.form.resetPassword')}
      isFormDisabled={isApiActionInProgress}
      onSubmit={setPassword}
    />
  )
}

export default ResetPasswordForm
