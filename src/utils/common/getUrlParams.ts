const getUrlParams = (paramNames: string[]) => {
  const url = new URL(window.location.href)
  return paramNames.map(paramName => url.searchParams.get(paramName) || undefined)
}

export default getUrlParams
