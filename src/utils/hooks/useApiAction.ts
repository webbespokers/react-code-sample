import React from 'react'
import { useServices } from 'services'
import Api, { ApiError } from 'services/Api'

/**
 * Hook used to call API action and handle an error.
 */
const useApiAction = <T = any>(
  action: (api: Api, abortSignal: AbortSignal) => Promise<T>,
): [() => Promise<void>, boolean, ApiError | void, T | void] => {
  const { api } = useServices()
  const abortController = React.useRef<AbortController>()
  const [error, setError] = React.useState<ApiError | void>(undefined)
  const [isRequestInProgress, setIsRequestInProgress] = React.useState(false)
  const [apiResult, setApiResult] = React.useState<T | void>(undefined)

  const doAction = async () => {
    try {
      if (isRequestInProgress) {
        abortController.current?.abort()
      }
      abortController.current = new AbortController()
      setApiResult(undefined)
      setError(undefined)
      setIsRequestInProgress(true)
      const result = await action(api, abortController.current.signal)
      setApiResult(result)
    } catch (exception) {
      setError(exception)
    } finally {
      setIsRequestInProgress(false)
    }
  }

  return [doAction, isRequestInProgress, error, apiResult]
}

export default useApiAction
